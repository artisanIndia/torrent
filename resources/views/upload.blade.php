<html>
<head>
    <title>Upload Data file</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
{!! Form::open(['files' => 'true']) !!}
<div class="form-group">
    <input type="file" class="form-group" name="file">
    <input type="submit" class="btn btn-success">
</div>
{!! Form::close() !!}
</body>
</html>