<html>
<head>
    <title>
        Details of connected peers
    </title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>IP Address</th>
            <th>Country</th>
            <th>City</th>
            <th>Resolved Name</th>
            <th>Packets In</th>
            <th>Average In</th>
            <th>Total In</th>
            <th>Average Size</th>
            <th>Packets Out</th>
            <th>Average Out</th>
            <th>Total Out</th>
        </tr>
        </thead>
        <tbody>
        @foreach($results as $result)
        <tr>
            <td>{{$result->ip_address}}</td>
            <td>{{getLocation($result->id,$result->ip_address, 'country')}}</td>
            <td>{{getLocation($result->id,$result->ip_address,'city')}}</td>
            <td>{{$result->resolved_name}}</td>
            <td>{{$result->active_packets_in}}</td>
            <td>{{$result->average_in}}</td>
            <td>{{$result->total_in}}</td>
            <td>{{$result->avg_size_in}}</td>
            <td>{{$result->active_packets_out}}</td>
            <td>{{$result->avg_size_out}}</td>
            <td>{{$result->packets_out}}</td>
        </tr>
            @endforeach
        </tbody>
    </table>
    {{$results->render()}}
</div>
</body>
</html>