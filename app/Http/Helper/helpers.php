<?php
    function getLocation($id,$ip, $value)
    {
        $validate = App\Address::where('id',$id)->first();
        if(count($validate) < 1)
        {
            $result = unserialize(\Mattbrown\Laracurl\Facades\Laracurl::get('http://ip-api.com/php/' . $ip));
            $validate = new App\Address;
            if(isset($result['city']))
            {
                $validate->city = $result['city'];
            }
            else
            {
                $validate->city = 'N/A';
            }
            if(isset($result['country']))
            {
                $validate->country = $result['country'];
            }
            else
            {
                $validate->country = 'N/A';
            }
            $validate->information_id = $id;
            $validate->save();
            if(isset($result[$value]))
            {
                return $result[$value];
            }
            return 'N/A';
        }
        return $validate[$value];
    }