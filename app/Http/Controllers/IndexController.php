<?php

namespace App\Http\Controllers;

use App\File;
use App\Information;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Torann\GeoIP\GeoIP;


class IndexController extends Controller
{
    public function showUpload()
    {
        return view('upload');
    }

    public function processUpload()
    {
        if(Input::hasFile('file'))
        {
            $file = Input::file('file');
            $allowedext = ["xml"];
            $original_name = $file->getClientOriginalName();
            $destinationPath = public_path('files');
            $filename = str_random(12);
            $extension = $file->getClientOriginalExtension();
            if(in_array($extension, $allowedext))
            {
                $file->move($destinationPath, $filename . '.' . $extension);
            }
            $xml = simplexml_load_file(public_path('files/'.$filename.'.'.$extension));
            $array = json_decode(json_encode((array)$xml), TRUE);

            foreach($array['nodes']['node'] as $node)
            {
                if(isset($node['name']['id']['IP']))
                {
                    $instance = new Information();
                    $instance->ip_address = $node['name']['id']['IP'];
                    $instance->resolved_name = $node['name']['resolved_name'];
                    $instance->average_in = $node['traffic_stats']['in']['stats']['avg'].'<br />';
                    $instance->total_in = $node['traffic_stats']['in']['stats']['total'].'<br />';
                    $instance->avg_size_in = $node['traffic_stats']['in']['stats']['avg_size'].'<br />';
                    $instance->packets_in = $node['traffic_stats']['in']['stats']['packets'].'<br />';
                    $instance->average_out =  $node['traffic_stats']['out']['stats']['avg'].'<br />';
                    $instance->total_out = $node['traffic_stats']['out']['stats']['total'].'<br />';
                    $instance->avg_size_out = $node['traffic_stats']['out']['stats']['avg_size'].'<br />';
                    $instance->packets_out = $node['traffic_stats']['out']['stats']['packets'].'<br />';
                    $instance->save();
                }
            }
        }
        else
        {
            return redirect()->back()->with('message', 'success');
        }
    }

    public function details()
    {
        $results = Information::whereRaw("ip_address NOT like '192.168%' and ip_address NOT like '255.255%' and ip_address NOT like '10.240%'")->paginate(30);
        return view('details', compact('results'));
    }
}
