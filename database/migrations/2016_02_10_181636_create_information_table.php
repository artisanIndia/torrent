<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address');
            $table->text('resolved_name');
            $table->integer('active_packets_in');
            $table->integer('average_in');
            $table->integer('total_in');
            $table->integer('avg_size_in');
            $table->integer('packets_in');
            $table->integer('active_packets_out');
            $table->integer('average_out');
            $table->integer('total_out');
            $table->integer('avg_size_out');
            $table->integer('packets_out');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('information');
    }
}
